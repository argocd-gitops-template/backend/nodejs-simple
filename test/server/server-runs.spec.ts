import { app } from "../server";
import request from "supertest";

describe("server checks", function () {
  it("server is created without error", function (done) {
    request(app).get("/generate_204").expect(204, done);
  });
});
