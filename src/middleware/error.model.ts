export enum ErrorStatus {
  OK = 200,
  NO_CONTENT = 204,
  INTERNAL_SERVER_ERROR = 500,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  NOT_FOUND = 404,
  CONFLICT = 409,
  FORBIDDEN = 403,
}

export class ErrorModel {
  message!: string;
  status!: number;
  additionalInfo!: any;

  constructor(
    message: string,
    status: ErrorStatus = ErrorStatus.INTERNAL_SERVER_ERROR,
    additionalInfo: any = {}
  ) {
    this.message = message;
    this.status = status;
    this.additionalInfo = additionalInfo;
  }
}
